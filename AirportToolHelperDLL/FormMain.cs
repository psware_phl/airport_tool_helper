﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using System.Threading;
using System.Diagnostics;

namespace AirportToolHelperDLL
{
    public partial class FormMain : Form
    {
        private const string swRevision = "1.0.1.3";

        private string[] THR_TYPE = { "threshold", "displaced threshold" };
        private const double NULL_VALUE = -32764.00;
        private const double WATER_NULL_VALUE = -32765.00;
        private const int THRESHOLD = 0;
        private const int DISPLACED_THRESHOLD = 1;

        private string gblWorkSpace = "";
        private string gblVectorFile = "";

        public FormMain()
        {
            InitializeComponent();
            initializeOtherComponent();

            this.Text = "AirportToolHelperEX\u00AE v" + swRevision;
        }

        private void initializeOtherComponent()
        {
            textBoxFile.DragEnter += new DragEventHandler(textBoxFile_DragEnter);
            textBoxFile.DragDrop += new DragEventHandler(textBoxFile_DragDrop);
            this.Shown += new EventHandler(FormMain_Shown);
        }

        void FormMain_Shown(object sender, EventArgs e)
        {
            textBoxFile.Focus();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void textBoxFile_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        void textBoxFile_DragDrop(object sender, DragEventArgs e)
        {
            textBoxFile.AllowDrop = false;
            try
            {
                string workspace;
                bool validFileFound = false;
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                if (files != null && files.Count() > 0)
                {
                    if (files.Count() > 1)
                    {
                        MessageBox.Show(this, "Multiple files detected. The program will process only the first valid file.",
                            "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                    foreach (string file in files)
                    {
                        if (file.EndsWith(".vec", StringComparison.OrdinalIgnoreCase))
                        {                            
                            validFileFound = true;

                            workspace = file.Substring(0, file.Length - 4) + ".wrk";
                            if (!File.Exists(workspace))
                            {
                                DialogResult result = MessageBox.Show(this, "Workspace " + Path.GetFileName(workspace) + " not found.\n" +
                                                                            "Do you want to continue? ", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                                if (DialogResult.Yes == result)
                                {
                                    gblWorkSpace = workspace;
                                    gblVectorFile = file;
                                    checkXMLFile(file, "");
                                }
                            }
                            else
                            {
                                gblWorkSpace = workspace;
                                gblVectorFile = file;
                                checkXMLFile(file, workspace);
                            }
                            break;
                        }
                    }

                    if (!validFileFound)
                    {
                        MessageBox.Show(this, "No valid VEC file found. Validation cancelled", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                String errorMessage = "Error encountered\n\n" +
                                      "Error details: \n" +
                                      ex.Message;
                MessageBox.Show(this, errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                textBoxFile.AllowDrop = true;
            }
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            if (gblVectorFile != null && gblVectorFile != "")
            {
                checkXMLFile(gblVectorFile, gblWorkSpace);
            }
        }

        private void checklistToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("HelperChecklist.xlsx");
        }

        private void checkXMLFile(string vecFile, string workspace)
        {
            using (FormBusy busyForm = new FormBusy())
            {
                busyForm.Show(this);
                Thread.Sleep(10);
                FileInfo info = new FileInfo(vecFile);
                textBoxFile.Text = vecFile + Environment.NewLine +
                    "File size: " + info.Length + " bytes" + Environment.NewLine +
                    "Last modified: " + info.LastWriteTime.ToString();
                richTextBoxMessage.Clear();
                toolStripStatusLabel.Text = "Starting";
                Application.DoEvents();
                performXMLFileChecking(vecFile, workspace);
                toolStripStatusLabel.Text = "Finish";
            }
        }

        private void performXMLFileChecking(string vecFile, string workspace)
        {
            XDocument vecXMLFile = XDocument.Load(vecFile, LoadOptions.PreserveWhitespace);
            XDocument wrkXMLFile = (workspace == "" ? null : XDocument.Load(workspace, LoadOptions.PreserveWhitespace));

            toolStripStatusLabel.Text = "Checking runways";
            checkRunway(vecXMLFile);

            toolStripStatusLabel.Text = "Checking parkings";
            checkParking(vecXMLFile);

            toolStripStatusLabel.Text = "Checking taxiways";
            checkTaxiway(vecXMLFile);

            toolStripStatusLabel.Text = "Checking vertical structures";
            checkVerticalPolyStructure(vecXMLFile);

            toolStripStatusLabel.Text = "Checking metadata";
            checkMetadata(vecXMLFile);

            toolStripStatusLabel.Text = "Checking horizontal accuracy";
            checkHorizontalAccuracy(vecXMLFile, wrkXMLFile);

            if (richTextBoxMessage.Text.Equals(""))
            {
                richTextBoxMessage.AppendText("No error found.\n");
            }
        }

        #region Check runway

        private void checkRunway(XDocument xmlFile)
        {
            string runwayName;
            IEnumerable<XElement> query;
            List<string> listOfStopway = new List<string>();

            query = from c in xmlFile.Element("ITEMS").Elements("Runway")
                    select c;
            foreach (XElement runway in query)
            {
                listOfStopway.Clear();

                runwayName = runway.Attribute("idobject").Value;
                checkRunwayDerivedEdge(runwayName, runway);
                checkRunwayThreshold(runwayName, runway, ref listOfStopway);
                checkRunwayStopway(runwayName, runway, listOfStopway);
                checkRunwayMarking(runwayName, runway);
                checkRunwayCenterline(runwayName, runway);
            }
        }

        private void checkRunwayDerivedEdge(string runwayName, XElement runway)
        {
            IEnumerable<XElement> query;

            query = from c in runway.Descendants("AsrnEdge")
                    where !c.Attribute("edgederv").Value.Equals("2")
                    select c;
            foreach (XElement edge in query)
            {
                richTextBoxMessage.AppendText("Warning: Runway " + runwayName + " has non-derived edge.\n");
            }
        }

        private void checkRunwayThreshold(string runwayName, XElement runway, ref List<string> listOfStopway)
        {
            bool stopwayDeclared;

            foreach (XElement thr in runway.Elements("RunwayThreshold"))
            {
                checkRunwayDeclaredDistances(thr, out stopwayDeclared);
                if (stopwayDeclared)
                {
                    listOfStopway.Add(thr.Attribute("idobject").Value);
                }
            }
        }

        private void checkRunwayDeclaredDistances(XElement threshold, out bool stopwayDeclared)
        {
            string thrID, thrType;
            int type;
            double tora, toda, asda, lda;
            bool checkStopway = true;

            stopwayDeclared = false;
            thrID = threshold.Attribute("idobject").Value;
            type = Convert.ToInt32(threshold.Attribute("thrtype").Value);
            thrType = THR_TYPE[type];
            tora = Convert.ToDouble(threshold.Attribute("tora").Value);
            toda = Convert.ToDouble(threshold.Attribute("toda").Value);
            asda = Convert.ToDouble(threshold.Attribute("asda").Value);
            lda = Convert.ToDouble(threshold.Attribute("lda").Value);

            if (NULL_VALUE == tora)
            {
                checkStopway = false;
                richTextBoxMessage.AppendText("Warning: Runway " + thrID + " " + thrType + " TORA value is null.\n");
            }

            if (NULL_VALUE == toda)
            {
                richTextBoxMessage.AppendText("Warning: Runway " + thrID + " " + thrType + " TODA value is null.\n");
            }

            if (NULL_VALUE == asda)
            {
                checkStopway = false;
                richTextBoxMessage.AppendText("Warning: Runway " + thrID + " " + thrType + " ASDA value is null.\n");
            }

            if (NULL_VALUE == lda)
            {
                richTextBoxMessage.AppendText("Warning: Runway " + thrID + " " + thrType + " LDA value is null.\n");
            }

            if (THRESHOLD == type)
            {
                double cat = Convert.ToDouble(threshold.Attribute("cat").Value);
                if (NULL_VALUE == cat)
                {
                    richTextBoxMessage.AppendText("Warning: Runway " + thrID + " " + thrType + " category not specified.\n");
                }

                if (checkStopway && (asda - tora > 0))
                {
                    stopwayDeclared = true;
                }
            }
        }

        private void checkRunwayStopway(string runwayName, XElement runway, List<string> listOfStopway)
        {
            string stpID;

            foreach (XElement stpway in runway.Elements("Stopway"))
            {
                stpID = stpway.Attribute("idobject").Value;
                if (!listOfStopway.Contains(stpID))
                {
                    richTextBoxMessage.AppendText("Warning: Runway " + stpID + " should have no stopway.\n");
                }
                else
                {
                    listOfStopway.Remove(stpID);
                }
            }

            foreach (string rn in listOfStopway)
            {
                richTextBoxMessage.AppendText("Warning: Runway " + rn + " should have a stopway.\n");
            }
        }

        private void checkRunwayMarking(string runwayName, XElement runway)
        {
            IEnumerable<XElement> query;

            query = from c in runway.Elements("RunwayMarking")
                    where Convert.ToDouble(c.Attribute("rwymktyp").Value) == NULL_VALUE
                    select c;
            foreach (XElement marking in query)
            {
                richTextBoxMessage.AppendText("Warning: Runway " + runwayName + " has unknown marking.\n");
            }
        }

        private void checkRunwayCenterline(string runwayName, XElement runway)
        {
            IEnumerable<XElement> query;

            query = from c in runway.Element("PaintedCenterline").Elements("Line")
                    select c;
            if (query.Count() != 1)
            {
                richTextBoxMessage.AppendText("Warning: Runway " + runwayName + " has multiple centerline.\n");
            }
            else
            {
                char[] separator = { ' ' };
                string[] vPoints = query.First().Value.Split(separator, StringSplitOptions.None);
                if (vPoints.Count() > 4)
                {
                    richTextBoxMessage.AppendText("Warning: Runway " + runwayName + " centerline has more than 2 vertices.\n");
                }
            }
        }

        #endregion

        #region Check parking stand

        private void checkParking(XDocument xmlFile)
        {
            string parkingName, apronName, apronPCN;
            IEnumerable<XElement> query;

            List<string> vPolyList = xmlFile.Element("ITEMS").Elements("VerticalPolygonal").ToList()
                .Select(element => element.Attribute("idobject").Value).ToList();

            Dictionary<string, string> apronList = new Dictionary<string, string>();
            query = from c in xmlFile.Element("ITEMS").Descendants("ApronElement")
                    select c;
            foreach (XElement element in query)
            {
                apronName = element.Attribute("idobject").Value;
                apronPCN = element.Attribute("pcn").Value;
                if (apronList.ContainsKey(apronName))
                {
                    apronList[apronName] += "," + apronPCN;
                }
                else
                {
                    apronList.Add(apronName, apronPCN);
                }
            }

            query = from c in xmlFile.Element("ITEMS").Elements("Parking")
                    select c;
            foreach (XElement parking in query)
            {
                parkingName = parking.Attribute("idobject").Value;
                if (!vPolyList.Contains(parkingName) && !parkingName.Equals("$UNK", StringComparison.OrdinalIgnoreCase))
                {
                    richTextBoxMessage.AppendText("Warning: Parking " + parkingName + " does not match with any of the terminal building.\n");
                }
                checkParkingStandArea(parkingName, parking, apronList);
                checkStandGuidanceLine(parkingName, parking);
                checkParkingStandLocation(parkingName, parking);
                checkParkingNodes(parkingName, parking);
            }
        }

        private void checkParkingStandArea(string parkingName, XElement parking, Dictionary<string, string> apronList)
        {
            string standName, standPCN, apronName;
            char[] separator1 = { ' ' };
            char[] separator2 = { ',' };
            string[] tokens;
            List<string> apronPCNs;
            IEnumerable<XElement> query;

            query = from c in parking.Descendants("ParkingStandArea")
                    select c;
            foreach (XElement area in query)
            {
                standName = area.Attribute("idobject").Value;

                tokens = standName.Split(separator1, StringSplitOptions.RemoveEmptyEntries);
                if (tokens.Count() != 1)
                {
                    richTextBoxMessage.AppendText("Warning: Invalid " + standName + " parking stand area name.\n");
                }

                if (!area.Attribute("terminalref").Value.Equals(parkingName))
                {
                    richTextBoxMessage.AppendText("Warning: Invalid " + standName + " parking stand area terminal ref.\n");
                }

                standPCN = area.Attribute("pcn").Value;
                apronName = area.Attribute("idapron").Value;
                if (!apronList.ContainsKey(apronName))
                {
                    richTextBoxMessage.AppendText("Warning: Invalid " + standName + " parking stand area apron ID.\n");
                }
                else
                {
                    apronPCNs = apronList[apronName].Split(separator2, StringSplitOptions.RemoveEmptyEntries).ToList();
                    if (!apronPCNs.Contains(standPCN))
                    {
                        richTextBoxMessage.AppendText("Warning: " + standName + " parking stand and " + apronName + " apron PCN mismatch.\n");
                    }
                }
            }
        }

        private void checkStandGuidanceLine(string parkingName, XElement parking)
        {
            string standName;
            char[] separator1 = { ' ' };
            string[] tokens;
            IEnumerable<XElement> query;
            IEnumerable<XElement> standLines = parking.Descendants("StandGuidanceLine");

            foreach (XElement standGuidanceLine in standLines)
            {
                standName = standGuidanceLine.Attribute("idobject").Value;

                tokens = standName.Split(separator1, StringSplitOptions.RemoveEmptyEntries);
                if (tokens.Count() != 1)
                {
                    richTextBoxMessage.AppendText("Warning: Invalid " + standName + " stand guidance line name.\n");
                }
            }

            query = from c in standLines
                    where !c.Attribute("terminalref").Value.Equals(parkingName)
                    select c;
            foreach (XElement standGuidanceLine in query)
            {
                standName = standGuidanceLine.Attribute("idobject").Value;
                richTextBoxMessage.AppendText("Warning: Invalid " + standName + " stand guidance line terminal ref.\n");
            }

            query = from c in standLines
                    where !c.Attribute("direc").Value.Equals("0")
                    select c;
            foreach (XElement standGuidanceLine in query)
            {
                standName = standGuidanceLine.Attribute("idobject").Value;
                richTextBoxMessage.AppendText("Warning: " + standName + " stand guidance line should be bi-directional.\n");
            }
        }

        private void checkParkingStandLocation(string parkingName, XElement parking)
        {
            string standName;
            IEnumerable<XElement> query;

            query = from c in parking.Descendants("ParkingStandLocation")
                    where !c.Attribute("terminalref").Value.Equals(parkingName)
                    select c;
            foreach (XElement area in query)
            {
                standName = area.Attribute("idobject").Value;
                richTextBoxMessage.AppendText("Warning: Invalid " + standName + " parking stand location terminal ref.\n");
            }
        }

        private void checkParkingNodes(string parkingName, XElement parking)
        {
            IEnumerable<XElement> query;

            query = from c in parking.Descendants("AsrnNode")
                    where c.Attribute("nodetype").Value.Equals("5")
                    select c;
            foreach (XElement edge in query)
            {
                richTextBoxMessage.AppendText("Warning: Parking " + parkingName + " has parking entry/exit node.\n");
            }
        }

        #endregion

        #region Check taxiway

        private void checkTaxiway(XDocument xmlFile)
        {
            string taxiName;
            IEnumerable<XElement> query;

            List<string> runwayList = xmlFile.Element("ITEMS").Elements("Runway").ToList()
                .Select(element => element.Attribute("idobject").Value).ToList();

            query = from c in xmlFile.Element("ITEMS").Descendants("Taxiway")
                    select c;
            foreach (XElement taxiway in query)
            {
                taxiName = taxiway.Attribute("idobject").Value;
                checkTaxiwayDerivedEdge(taxiName, taxiway);
                checkDuplicatePositivePoly(taxiName, taxiway);
                checkTaxiwayEdgeID(taxiName, taxiway, runwayList);
                checkTaxiwayNodes(taxiName, taxiway);
            }
        }

        private void checkDuplicatePositivePoly(string taxiName, XElement taxiway)
        {
            int polyCount;
            IEnumerable<XElement> query;

            query = from c in taxiway.Elements("TaxiwayElement")
                    where c.Attribute("runwayexit").Value == "1" &&
                          c.Elements("Polygon").Count() > 1
                    select c;
            foreach (XElement taxiwayElement in query)
            {
                polyCount = taxiwayElement.Elements("Polygon").Count();
                richTextBoxMessage.AppendText("Warning: Taxiway " + taxiName + " runway exit container has more than 1 polygon.\n");
            }
        }

        private void checkTaxiwayDerivedEdge(string taxiName, XElement taxiway)
        {
            IEnumerable<XElement> query;

            query = from c in taxiway.Descendants("AsrnEdge")
                    where !c.Attribute("edgederv").Value.Equals("2")
                    select c;
            foreach (XElement edge in query)
            {
                richTextBoxMessage.AppendText("Warning: Taxiway " + taxiName + " has non-derived edge.\n");
            }
        }

        private void checkTaxiwayEdgeID(string taxiName, XElement taxiway, List<string> runwayList)
        {
            char[] separator = { '_' };
            string[] name;

            foreach (XElement edge in taxiway.Descendants("AsrnEdge"))
            {
                name = edge.Attribute("idobject").Value.Split(separator, StringSplitOptions.RemoveEmptyEntries);
                switch (name.Count())
                {
                    case 1:
                        if (name[0] != taxiName)
                        {
                            if (!runwayList.Contains(name[0]))
                            {
                                richTextBoxMessage.AppendText("Warning: Taxiway " + taxiName + " has invalid edge name.\n");
                            }
                        }
                        break;
                    case 2:
                        if (name[0] != taxiName && name[1] != taxiName)
                        {
                            richTextBoxMessage.AppendText("Warning: Taxiway " + taxiName + " has invalid edge name.\n");
                        }
                        break;
                }
            }
        }

        private void checkTaxiwayNodes(string taxiName, XElement taxiway)
        {
            IEnumerable<XElement> query;

            query = from c in taxiway.Descendants("AsrnNode")
                    where c.Attribute("nodetype").Value.Equals("7")
                    select c;
            foreach (XElement edge in query)
            {
                richTextBoxMessage.AppendText("Warning: Taxiway " + taxiName + " has link node.\n");
            }
        }

        #endregion

        #region Check vertical polygon structure

        private void checkVerticalPolyStructure(XDocument xmlFile)
        {
            string vPolyName, vPolyStructName;
            double polyType;
            IEnumerable<XElement> query;

            query = from c in xmlFile.Element("ITEMS").Elements("VerticalPolygonal")
                    select c;
            foreach (XElement vPoly in query)
            {
                vPolyName = vPoly.Attribute("idobject").Value;
                foreach (XElement vPolyStructure in vPoly.Elements("VerticalPolygonalStructure"))
                {
                    vPolyStructName = vPolyStructure.Attribute("idobject").Value;
                    polyType = Convert.ToDouble(vPolyStructure.Attribute("plysttyp").Value);

                    if (vPolyStructName != vPolyName)
                    {
                        richTextBoxMessage.AppendText("Warning: Vertical polygon " + vPolyName + " container name mismatch.\n");
                    }

                    if (NULL_VALUE == polyType)
                    {
                        richTextBoxMessage.AppendText("Warning: Vertical structure " + vPolyStructName + " type should not be unknown.\n");
                    }
                }
            }
        }

        #endregion

        #region Check metadata

        private void checkMetadata(XDocument xmlFile)
        {
            if (xmlFile.Element("ITEMS").Attribute("elev").Value.Equals("0"))
            {
                richTextBoxMessage.AppendText("Warning: Aiport elevation is zero.\n");
            }
        }

        #endregion

        #region Check horizontal accuracy

        private void checkHorizontalAccuracy(XDocument vecXMLFile, XDocument wrkXMLFile)
        {           
            try
            {                
                if (wrkXMLFile == null)
                {
                    MessageBox.Show(this, "HACC checking will not be peformed because workspace is missing", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {

                    string elementName, objectID;
                    double whacc, vhacc;
                    IEnumerable<XElement> query;
                    XAttribute attrib;
                    XElement haccNode = wrkXMLFile.Element("ITEMS").Element("HACC");

                    whacc = Convert.ToDouble(haccNode.Value);
                    query = from c in vecXMLFile.Element("ITEMS").Descendants()
                            where c.Attribute("hacc") != null
                            select c;
                    foreach (XElement element in query)
                    {
                        elementName = element.Name.LocalName;
                        vhacc = Convert.ToDouble(element.Attribute("hacc").Value);
                        if (elementName.Equals("Water", StringComparison.OrdinalIgnoreCase))
                        {
                            if (WATER_NULL_VALUE != vhacc && whacc != vhacc)
                            {
                                richTextBoxMessage.AppendText("Warning: " + elementName + " hacc is invalid.\n");
                            }
                            else
                            {
                                richTextBoxMessage.AppendText("Note: " + elementName + " has hacc of " + vhacc + ".\n");
                            }
                        }
                        else if (elementName.Equals("Hotspot", StringComparison.OrdinalIgnoreCase))
                        {
                            objectID = element.Attribute("idobject").Value;
                            if (NULL_VALUE != vhacc && whacc != vhacc)
                            {
                                richTextBoxMessage.AppendText("Warning: " + elementName + " " + objectID + " hacc is invalid.\n");
                            }
                            else
                            {
                                richTextBoxMessage.AppendText("Note: " + elementName + " " + objectID + " has hacc of " + vhacc + ".\n");
                            }
                        }
                        else if (elementName.Equals("DeicingArea", StringComparison.OrdinalIgnoreCase))
                        {
                            if (NULL_VALUE != vhacc && whacc != vhacc)
                            {
                                richTextBoxMessage.AppendText("Warning: " + elementName + " hacc is invalid.\n");
                            }
                            else
                            {
                                richTextBoxMessage.AppendText("Note: " + elementName + " has hacc of " + vhacc + ".\n");
                            }
                        }
                        else
                        {
                            if (!vhacc.Equals(whacc))
                            {
                                attrib = element.Attribute("idobject");
                                objectID = attrib == null ? "" : element.Attribute("idobject").Value;
                                richTextBoxMessage.AppendText("Warning: " + elementName + " " + objectID + " hacc not set to " + whacc + ".\n");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                String errorMessage = "Error encountered\n\n" +
                                      "Error details: \n" +
                                      ex.Message;
                MessageBox.Show(this, errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion
    }
}
